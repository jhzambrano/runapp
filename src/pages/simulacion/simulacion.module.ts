import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SimulacionPage } from './simulacion';

@NgModule({
  declarations: [
    SimulacionPage,
  ],
  imports: [
    IonicPageModule.forChild(SimulacionPage),
  ],
})
export class SimulacionPageModule {}
