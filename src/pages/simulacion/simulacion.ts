import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { TowerProvider } from '../../providers/tower/tower';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the SimulacionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-simulacion',
  templateUrl: 'simulacion.html',
})
export class SimulacionPage {
  slideOneForm: FormGroup;
  items = [];
  athlete_name=[];
  hide:boolean = true;
  hide2:boolean=false;
  public quotesArray: any[] = [];
  public randomQuote: string;
  athlete: string;
  
  end:number;
  constructor(public navCtrl: NavController, public navParams: NavParams, public tower: TowerProvider,public formBuilder: FormBuilder,public loadingCtrl: LoadingController) {  
    this.slideOneForm = formBuilder.group({     
      athlete: ['', Validators.compose([Validators.required])],     
  
    });
               
    
  }
  simular()
  {
    this.hide = !this.hide;
    this.hide2=!this.hide2;  
    this.presentLoadingDefault();    
    this.getData();
    
  }
  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      content: 'Waiting for tower...'
    });
  
    loading.present();
  
    setTimeout(() => {      
      loading.dismiss();      
    }, 5000);        
  }
  getData()
  { this.athlete_name.push(this.athlete)
    this.items=[
      {
       'title': '10 yd split:                ',
       'secs': '1sec',
       'id':'1'      
      },
      {
        'title': '20 yd split:                ',
        'secs': '1sec',
        'id':'2'
       },
       {
        'title': '30 yd split:                ',
        'secs': '3sec',
        'id':'1'
       },
       {
        'title': '40 yd split:                ',
        'secs': '2sec',
        'id':'1'
       },
       {
        'title': 'Total time :                ',
        'secs': '7sec',
        'id':'1'
       },
      ]
      for(let i=0;i<this.items.length;i++)
      this.quotesArray.push(
        {"cadena":this.items[i].title+"               "+this.items[i].secs,"numero":i}           
        
      );        
    let valor=0;
      var mytimer=setInterval(() => {        
        this.items[valor] = this.quotesArray[valor]; // this'll get the quote depending on your array length}
        valor++;      
        console.log(this.items)  
        if(valor==5)
          clearInterval(mytimer);
      }, 2000);     
  }
}
