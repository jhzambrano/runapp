import { Component, NgZone } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';
import { TowerProvider } from '../../providers/tower/tower';
import { HomePage} from '../../pages/home/home';

@Component({
  selector: 'page-home',
  templateUrl: 'ble.html'
})

export class BlePage {

  unpairedDevices: any[];
  pairedDevices: any;
  gettingDevices: Boolean;
  croDebug: String;
  appConnectedTo: string;
  pData: string;

  constructor(private alertCtrl: AlertController, private ble: BLE, public tower: TowerProvider, private NC: NavController, private zone: NgZone) {
    
  }

  startScanning() {
    
    this.unpairedDevices = [];
    this.croDebug += "Starting Scan.. \n <br>";
    this.tower.scanDevices().subscribe(  
      (someDevice) => {
        this.zone.run(() => {
          this.unpairedDevices.push(someDevice);
        });
        this.croDebug = "Found:  " + JSON.stringify(someDevice) + " \n" + this.croDebug;

      },

      (error) => {
        alert( 'SCAN ERROR: ' + JSON.stringify(error) );
        this.croDebug += 'SCAN ERROR: ' + JSON.stringify(error) + "\n";
      },
      () => {
        alert("complete: " + JSON.stringify(this.unpairedDevices));
        this.croDebug += "Scan finished/completed \n";
      }
    );
  }

  connectTo(device){
    this.alertCtrl.create({
      title: 'Connect',
      message: 'Do you want to connect with ( '+ (device.name || device.id) +' ) ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Connect',
          handler: () => {

           this.tower.connect(device.id).subscribe( peripheralData => {
             this.tower.isConnected = true;
             this.tower.deviceId = device.id;
             this.tower.peripheralData = peripheralData;
             this.tower.startReceiving();
             console.log(JSON.stringify(this.tower.peripheralData));

            this.tower.determineWriteType();
              this.NC.push(HomePage);
             //this.arduinoSubscribe();
           }, peripheralData =>{
             this.tower.resetInfo();
             alert('Tower connection problem.');
           } )
          }
        }
      ]
    }).present()
  }

  disconnect(){

    this.ble.disconnect( this.tower.deviceId ).then(
      value => {
        this.tower.resetInfo();
      }
    )
  }

  arduinoSubscribe(){
    this.ble.startNotification(this.tower.deviceId, this.tower.bluefruit.serviceUUID, this.tower.bluefruit.rxCharacteristic ).subscribe(

      info => {
        console.log(JSON.stringify(this.tower.bytesToString(info) ) );
      },
      error => {
        console.log( "error rx: " + error);
      }

    )
  }


}
