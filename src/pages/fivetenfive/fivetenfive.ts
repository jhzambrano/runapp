import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { TowerProvider } from '../../providers/tower/tower';
import { BLE } from '@ionic-native/ble';

/**
 * Generated class for the FivetenfivePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fivetenfive',
  templateUrl: 'fivetenfive.html',
})
export class FivetenfivePage {

  items: any[];
  athlete_name: string;
  towerLoading: any;
  trainingActive: boolean;
  trainingFinished: boolean;
  
  splitLabel: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public tower: TowerProvider,
    public loadingCtrl: LoadingController,
    private ble: BLE,
    private zone: NgZone
  ){
    this.trainingActive = false;
    this.trainingFinished = false;
    this.athlete_name = '';
    this.splitLabel = ['10 yd split', '20 yd split', '30 yd split', '40 yd split', '50 yd split'];
  }

  ionViewDidLoad() {
    console.log(this.tower.isConnected);
    console.log('ionViewDidLoad FivetenfivePage');
    this.arduinoSubscribe();
  }

  startTraining(){

    this.items = [];

    this.tower.write('2').then(
      value => {
        console.log('nrf8001 TX TX ' + JSON.stringify(value));
        this.tower.isOnUse = true;
        this.trainingActive = true;
        this.trainingFinished = false;
        this.towerLoading = this.loadingCtrl.create({
          content: 'Waiting for tower...'
        });

        this.towerLoading.present();
      },
      error => {
        console.log('error escritura!! TX!' + JSON.stringify(error))
      }
    );
  }

  arduinoSubscribe() {
    this.ble.startNotification(
      this.tower.deviceId,
      this.tower.bluefruit.serviceUUID,
      this.tower.bluefruit.rxCharacteristic
    ).subscribe(

      info => {
        var response = this.tower.bytesToString(info);
        var response_split = null;
        var response_interval = null;
        var time_diff = 0, current_time = 0;
        console.log("info 5-10-5 --> " + JSON.stringify(response));

        if(response == "0:1"){
          this.towerLoading.dismiss();
        }
        if( response.startsWith('2-') ){

          response_split = response.split(":");
          response_interval = parseInt( response_split[0].split('-')[1] );
          current_time = parseInt(response_split[1]);
          if (this.items.length == 0){
            time_diff = current_time;
          }else{
            time_diff = current_time - this.items[this.items.length - 1].time;
          }

          this.zone.run(() => {
            console.log(JSON.stringify(this.splitLabel));

            this.items.push({              
                split: response_interval,
                time: current_time,
                label: this.splitLabel[response_interval - 1] + '\t\t\t' +  time_diff
              });
            if (response_interval == 5) {
              this.items.push({
                split: 6,
                time: current_time,
                label: 'Total time: \t\t\t' + current_time
              });
              this.trainingFinished = true;
              this.tower.isOnUse = false;
            }
          });
          
          console.log('interval added -- ' + JSON.stringify({ split: response_interval, time: response_split[1] }));
        }

      },
      error => {
        console.log("error rx: " + error);
      }

    )
  }






}
