import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FivetenfivePage } from './fivetenfive';

@NgModule({
  declarations: [
    FivetenfivePage,
  ],
  imports: [
    IonicPageModule.forChild(FivetenfivePage),
  ],
})
export class FivetenfivePageModule {}
