import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YarddashPage } from './yarddash';

@NgModule({
  declarations: [
    YarddashPage,
  ],
  imports: [
    IonicPageModule.forChild(YarddashPage),
  ],
})
export class YarddashPageModule {}
