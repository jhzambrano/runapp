import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import{YarddashPage} from "../yarddash/yarddash"
import{FivetenfivePage} from "../fivetenfive/fivetenfive"
import{SettingsPage} from "../settings/settings"
import{BlePage} from "../ble/ble"


@Component({
  selector: 'homepage',
  templateUrl: 'home.html',
})
export class HomePage {
  items = [];

  constructor(public nav: NavController) {
    this.items = [
      {
        'title': '40 yard-dash',
        'icon': 'walk',
        'description': 'A powerful Javascript framework for building single page apps. Angular is open source, and maintained by Google.',
        'color': '#E63135',
        'id':1
      },
      {
        'title': '5-10-5',
        'icon': 'walk',
        'description': 'The latest version of cascading stylesheets - the styling language of the web!',
        'color': '#0CA9EA',
        'id':2
      },
      {
        'title': 'Tower Setup',
        'icon': 'bluetooth',
        'description': 'The latest version of the web\'s markup language.',
        'color': '#F46529',
        'id':4
      },
      
    ]
  }

  openNavDetailsPage(item) {        
    //se evalua el id de cada item para llamar una pagina en particular
    if(item['id']==1){
        //se llama la página de 40 yard dash
        this.nav.push(YarddashPage)
      }
      if(item['id']==2){
        //se llama la página de 40 yard dash
        this.nav.push(FivetenfivePage)
      }
      if(item['id']==3){
        //se llama la página de 40 yard dash
        this.nav.push(SettingsPage)
      }  
      if(item['id']==4){
        //se llama la página de 40 yard dash
        this.nav.push(BlePage)
      }  

  }

}