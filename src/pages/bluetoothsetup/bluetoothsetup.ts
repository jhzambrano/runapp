import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { AlertController } from 'ionic-angular';
@Component({
  selector: 'page-home',
  templateUrl: 'bluetoothsetup.html'
})
export class BluetoothsetupPage {

  unpairedDevices: any;
  pairedDevices: any;
  gettingDevices: Boolean;
  constructor(private bluetoothSerial: BluetoothSerial, private alertCtrl: AlertController) {
    bluetoothSerial.enable();
  }
  send(){       
   this.bluetoothSerial.isConnected().then(this.success,this.fail)   
    this.bluetoothSerial.subscribeRawData().subscribe(
      function(data){        
        //se debería mostrar la data, pero con el alert no se si de problema en iphone        
      }
    )    
  }
  startScanning() {    
    this.pairedDevices = null;
    this.unpairedDevices = null;
    this.gettingDevices = true;
    this.bluetoothSerial.discoverUnpaired().then((success) => {      
      this.unpairedDevices = success;
      this.gettingDevices = false;
      success.forEach(element => {
         
      });
    },
      (err) => {
        console.log("erroe:"+err);
      })

    this.bluetoothSerial.list().then((success) => {
      this.pairedDevices = success;
    },
      (err) => {
           
      })
  }
  success = (data) => alert(data);
  fail = (error) => alert(error);

  selectDevice(address: any) {
    let alert2 = this.alertCtrl.create({
      title: 'Connect',
      message: 'Do you want to connect with?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Connect',
          handler: () => {
            
            this.bluetoothSerial.isEnabled().then(res => {                                  
                    this.bluetoothSerial.connect(address).subscribe( this.success, this.fail);
                    
                    this.bluetoothSerial.isConnected().then(res => {                      
                     this.bluetoothSerial.write('algooooo')
                    }).catch(res => {                    
                      });
              
                  }).catch(res => {
                    
                  });
            
          }
        }
      ]
    });
    alert2.present();

  }

   connectSuccess(a)
  {
     
  }
   connectFailure(e)
  {
    
  }
  disconnect() {
    let alert = this.alertCtrl.create({
      title: 'Disconnect?',
      message: 'Do you want to Disconnect?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Disconnect',
          handler: () => {
            this.bluetoothSerial.disconnect();
          }
        }
      ]
    });
    alert.present();
  }
}
