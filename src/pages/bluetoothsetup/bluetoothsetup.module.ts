import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BluetoothsetupPage } from './bluetoothsetup';

@NgModule({
  declarations: [
    BluetoothsetupPage,
  ],
  imports: [
    IonicPageModule.forChild(BluetoothsetupPage),
  ],
})
export class BluetoothsetupPageModule {}
