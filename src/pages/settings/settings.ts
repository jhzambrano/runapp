import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import{BluetoothsetupPage} from "../bluetoothsetup/bluetoothsetup";

@Component({
  templateUrl: 'settings.html',
})
export class NavigationDetailsPage {
  item;

  constructor(params: NavParams) {
    this.item = params.data.item;    
    console.log("items"+this.item)
  }
}

@Component({
  template: `
<ion-header>
  <ion-navbar>
    <ion-title class='titulo'>Settings</ion-title>
  </ion-navbar>
</ion-header>
<ion-content>
  <ion-list>
    <button ion-item *ngFor="let item of items" (click)="openNavDetailsPage(item)" icon-start>
      <ion-icon [name]="item.icon" [ngStyle]="{'color': item.color}" item-start></ion-icon>
      {{ item.title }}
    </button>
  </ion-list>
</ion-content>
`
})
export class SettingsPage {
  items = [];

  constructor(public nav: NavController) {
    this.items = [
      {
        'title': 'Bluetooth Setup',
        'icon': 'bluetooth',
        'description': 'A powerful Javascript framework for building single page apps. Angular is open source, and maintained by Google.',
        'color': '#E63135',
        'id':1
      },
      {
        'title': 'Defaults',
        'icon': 'options',
        'description': 'The latest version of cascading stylesheets - the styling language of the web!',
        'color': '#0CA9EA',
        'id':2
      },      
      
    ]
  }

  openNavDetailsPage(item) {    
    console.log(item)
    //se evalua el id de cada item para llamar una pagina en particular
    if(item['id']==1){
        //se llama la página bluetooth setup
          this.nav.push(BluetoothsetupPage)
      }
      if(item['id']==2){
        //se llama la página defaults
        //this.nav.push(FivetenfivePage)
      }    
  }

}