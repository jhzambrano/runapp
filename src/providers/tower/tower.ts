import { BLE } from '@ionic-native/ble';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

/*
  Generated class for the TowerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TowerProvider {

  public isConnected: boolean;
  public deviceId: string;
  public writeWithoutResponse: boolean;
  public peripheralData: any;
  public batteryPerc: number;
  public batteryLowest: number;
  public isOnUse: boolean;

  public bluefruit = {
    serviceUUID: '6e400001-b5a3-f393-e0a9-e50e24dcca9e',
    txCharacteristic: '6e400002-b5a3-f393-e0a9-e50e24dcca9e', // transmit is from the phone's perspective
    rxCharacteristic: '6e400003-b5a3-f393-e0a9-e50e24dcca9e'  // receive is from the phone's perspective
  };

  constructor(private ble: BLE) {
    ble.enable();
    this.isOnUse = false;
    this.batteryPerc = null;
    //this.batteryPercentage();
    Observable.interval(5000).subscribe(x => {
      this.batteryPercentage();

      
      //this.batteryPercentageUpdate('10:' + Math.floor(Math.random() * 101) + '-' + Math.floor(Math.random() * 101) + '-' + Math.floor(Math.random() * 101) + '-' + Math.floor(Math.random() * 101))
    });

  }

  resetInfo(){
    this.isConnected = false;
    this.deviceId = null;
    this.writeWithoutResponse = false;
    this.peripheralData = null;
  }
  
  scanDevices(){
    
    return this.ble.scan([], 20);
    
  };

  connect(device){
    return this.ble.connect(device);
  }

  write(value){

    if (this.writeWithoutResponse){
      return this.ble.writeWithoutResponse(
        this.deviceId,
        this.bluefruit.serviceUUID,
        this.bluefruit.txCharacteristic,
        this.stringToBytes(value)
      )
    }else{
      return this.ble.write(
        this.deviceId,
        this.bluefruit.serviceUUID,
        this.bluefruit.txCharacteristic,
        this.stringToBytes(value)
      )
    }
    
  }


  determineWriteType() {
    // Adafruit nRF8001 breakout uses WriteWithoutResponse for the TX characteristic
    // Newer Bluefruit devices use Write Request for the TX characteristic

    var bluefruit = this.bluefruit; //tODO: quick trick for demo

    var characteristic = this.peripheralData.characteristics.filter(function (element) {
      if (element.characteristic.toLowerCase() === bluefruit.txCharacteristic) {
        return element;
      }
    })[0];

    if (characteristic.properties.indexOf('WriteWithoutResponse') > -1) {
      this.writeWithoutResponse = true;
    } else {
      this.writeWithoutResponse = false;
    }

  };

  startReceiving(){

    if(this.isConnected){
      /*
      this.ble.startNotification(
        this.deviceId,
        this.bluefruit.serviceUUID,
        this.bluefruit.rxCharacteristic
      ).subscribe(
        info => {
          var response = this.bytesToString(info);
          //Battery Percentage
          if (response.startsWith("10:")) {
            this.batteryPercentageUpdate(response);
          }
        }
      )*/
    }

    
  }

  disconnect(){

  }


  batteryPercentage(){
/*
    console.log('Battery Check');
    if(this.isOnUse == false && this.isConnected){
      this.write('10').then(
        value => {
          console.log('enviado');
        },
        error => {
          console.log('error enviando 10');
        }
      );
    }
*/
  }

  batteryPercentageUpdate( newVal ){

    console.log("newVal " + newVal)
    if(newVal == undefined)
      return;
    newVal = newVal.slice(3);
    console.log("newVal " + newVal);
    let batteries = newVal.split('-').map(parseFloat);
    let count = batteries.length;
    let sum = batteries.reduce((a, b) => a + b, 0);
    let avg = Math.floor(sum / count);
    let maxBat = Math.max(...batteries);
    let minBat = Math.min(...batteries);

    this.batteryPerc = avg;
    this.batteryLowest = minBat;
    console.log(avg);
    console.log(maxBat);
    console.log(minBat);

  }


  bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  };

  // ASCII only
  stringToBytes(string) {
    var array = new Uint8Array(string.length);
    for (var i = 0, l = string.length; i < l; i++) {
      array[i] = string.charCodeAt(i);
    }
    return array.buffer;
  };



}
