import { Component, NgZone } from '@angular/core';
import { TowerProvider } from '../../providers/tower/tower';
import { Observable } from 'rxjs/Rx';

/**
 * Generated class for the BatteryStatusComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'battery-status',
  templateUrl: 'battery-status.html'
})
export class BatteryStatusComponent {

  displayPercentage: number;
  batteryLowest: number;

  constructor(public tower: TowerProvider, private zone: NgZone) {
    Observable.interval(1000).subscribe(x => {
      this.displayPercentage = tower.batteryPerc;
      this.batteryLowest = tower.batteryLowest;
    });
  }

  displayWarning(){
    if (this.batteryLowest!=null && this.batteryLowest < 10){
      return true;
    }
    return false;
  }
 

}
