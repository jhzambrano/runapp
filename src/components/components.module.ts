import { NgModule } from '@angular/core';
import { BatteryStatusComponent } from './battery-status/battery-status';
@NgModule({
	declarations: [BatteryStatusComponent],
	imports: [],
	exports: [BatteryStatusComponent]
})
export class ComponentsModule {}