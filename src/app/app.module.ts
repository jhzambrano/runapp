import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { YarddashPage } from '../pages/yarddash/yarddash';
import { FivetenfivePage } from '../pages/fivetenfive/fivetenfive';
import { SettingsPage } from '../pages/settings/settings';
import { BluetoothsetupPage } from '../pages/bluetoothsetup/bluetoothsetup';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { SimulacionPage } from '../pages/simulacion/simulacion';
import { BlePage } from '../pages/ble/ble';
import { BLE } from '@ionic-native/ble';
import { TowerProvider } from '../providers/tower/tower';
import { BatteryStatusComponent } from '../components/battery-status/battery-status';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    YarddashPage,
    FivetenfivePage,
    SettingsPage,
    BluetoothsetupPage,
    SimulacionPage,
    BlePage,
    BatteryStatusComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    YarddashPage,
    FivetenfivePage,
    SettingsPage,
    BluetoothsetupPage,
    SimulacionPage,
    BlePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BluetoothSerial,
    BLE,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TowerProvider
  ]
})
export class AppModule {}
